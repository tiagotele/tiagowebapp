package com.tiagowebapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tiagowebapp.dao.ModeloGenericoTesteDAO;
import com.tiagowebapp.model.ModeloGenericoTeste;

@Controller
@Transactional
@RequestMapping("/novo")
public class NovoController {

	@Autowired
	ModeloGenericoTesteDAO modeloGenericoDAO;

	private final String path = "/novo";

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView home() {
		return list(null);
	}

	private ModelAndView list(String message) {
		List<ModeloGenericoTeste> modeloGenericoList = modeloGenericoDAO.list();
		ModelAndView modelAndView = new ModelAndView(path + "/index");
		modelAndView.addObject("modeloGenericoList", modeloGenericoList);
		if (message != null) {
			modelAndView.addObject("message", message);
		}
		return modelAndView;
	}

	@RequestMapping("/add")
	public ModelAndView add() {
		String message = "message";
		return new ModelAndView(path + "/add", "message", message);
	}

	@RequestMapping("/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModeloGenericoTeste modeloGenerico = modeloGenericoDAO.getModeloGenerico(id);
		ModelAndView modelAndView = new ModelAndView(path + "/edit");
		modelAndView.addObject("modeloGenerico", modeloGenerico);
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String save(ModeloGenericoTeste modeloGenericoTeste, RedirectAttributes redirectAttributes) {
		modeloGenericoDAO.saveModeloGenerico(modeloGenericoTeste);
		redirectAttributes.addFlashAttribute("message", "Dados adicionados com sucesso!");
		return "redirect:novo";
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(ModeloGenericoTeste modeloGenericoTeste, RedirectAttributes redirectAttributes) {
		modeloGenericoDAO.updateModeloGenerico(modeloGenericoTeste.getId(), modeloGenericoTeste);
		redirectAttributes.addFlashAttribute("message", "Dados atualizados com sucesso!");
		return "redirect:/novo";
	}

	@RequestMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		ModeloGenericoTeste modeloGenerico = modeloGenericoDAO.getModeloGenerico(id);
		modeloGenericoDAO.deleteModeloGenericoCity(modeloGenerico.getId());
		redirectAttributes.addFlashAttribute("message", "Dados deletados com sucesso!");
		return "redirect:/novo";
	}

}
