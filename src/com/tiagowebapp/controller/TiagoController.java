package com.tiagowebapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TiagoController {
	
	@RequestMapping("/")
	public ModelAndView home() {
		String message = "message";
		return new ModelAndView("index", "message", message);
	}
	
	@RequestMapping("/add")
	public ModelAndView add() {
		String message = "message";
		return new ModelAndView("add", "message", message);
	}
	
}
