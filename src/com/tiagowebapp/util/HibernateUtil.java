package com.tiagowebapp.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.tiagowebapp.model.ModeloGenericoTeste;

public class HibernateUtil {
	private static final SessionFactory sessionFactory;

	static {
		try {
			sessionFactory = new AnnotationConfiguration().configure().addPackage("com.tiagowebapp.model")
					.addAnnotatedClass(ModeloGenericoTeste.class).buildSessionFactory();
		} catch (Exception e) {
			System.err.println("Erro na criação do SessionFactory");
			throw new ExceptionInInitializerError(e);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
