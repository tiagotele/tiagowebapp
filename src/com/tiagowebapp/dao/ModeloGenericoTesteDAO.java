package com.tiagowebapp.dao;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.tiagowebapp.model.ModeloGenericoTeste;
import com.tiagowebapp.util.HibernateUtil;

@Repository
public class ModeloGenericoTesteDAO {

	public Long saveModeloGenerico(ModeloGenericoTeste objetoParaPersistir) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Long modeGenericoTesteId = null;
		try {
			transaction = session.beginTransaction();

			modeGenericoTesteId = (Long) session.save(objetoParaPersistir);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
		} finally {
			session.close();
		}

		return modeGenericoTesteId;
	}

	
	public List<ModeloGenericoTeste> list(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<ModeloGenericoTeste> modeGenericoList = null;
		try {
			transaction = session.beginTransaction();
			modeGenericoList = session.createQuery("from ModeloGenericoTeste").list();

			transaction.commit();

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return modeGenericoList;
	}

	public void updateModeloGenerico(Long modeGenericoTesteId, ModeloGenericoTeste modeloGenericoTeste) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			ModeloGenericoTeste modeloGenericoTesteAntigo = (ModeloGenericoTeste) session.get(ModeloGenericoTeste.class,
					modeGenericoTesteId);
			modeloGenericoTesteAntigo.setCampo1(modeloGenericoTeste.getCampo1());
			modeloGenericoTesteAntigo.setCampo2(modeloGenericoTeste.getCampo2());
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void deleteModeloGenericoCity(Long modeloGenericoTesteId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			ModeloGenericoTeste modeloGenericoTeste = (ModeloGenericoTeste) session.get(ModeloGenericoTeste.class,
					modeloGenericoTesteId);
			session.delete(modeloGenericoTeste);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public ModeloGenericoTeste getModeloGenerico(Long modeloGenericoTesteId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		ModeloGenericoTeste modeloGenericoTeste = null;
		try {
			transaction = session.beginTransaction();
			modeloGenericoTeste = (ModeloGenericoTeste) session.get(ModeloGenericoTeste.class,
					modeloGenericoTesteId);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return modeloGenericoTeste;
	}
}