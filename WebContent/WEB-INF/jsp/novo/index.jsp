<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tiago Web App - Novo index</title>
</head>
<link href="<c:url value='/resources/css/bootstrap.min.css'  />" rel="stylesheet" />
<link href="<c:url value='/resources/css/bootstrap-responsive.min.css'  />" rel="stylesheet" />
<script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
<body>

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse"
					data-target=".nav-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="brand" href="/TiagoWebApp">Tiago Web App</a>
<!-- 				<div class="nav-collapse collapse"> -->
<!-- 					<ul class="nav"> -->
<!-- 						<li class="active"><a href="#">Home</a></li> -->
<!-- 						<li><a href="#about">About</a></li> -->
<!-- 						<li><a href="#contact">Contact</a></li> -->
<!-- 					</ul> -->
<!-- 				</div> -->
<!-- 				/.nav-collapse -->
			</div>
		</div>
	</div>
	<div class="container">

		<!-- Main hero unit for a primary marketing message or call to action -->
		<div class="hero-unit">

			<h3>${message }</h3>

			<h3>
				<a href="/TiagoWebApp/novo/add" class="btn btn-primary btn-large">Add something on new controller</a>
			</h3>
		</div>
			<table class="table">
					<tr>
					<td>#</td>
					<td>Campo 1</td>
					<td>Campo 2</td>
					<td>Editar</td>
					<td>Deletar</td>
				</tr>
				<c:forEach items="${modeloGenericoList}" var="modeloGenerico">
					<tr>
						<td>${modeloGenerico.getId()}</td>
						<td>${modeloGenerico.getCampo1()}</td>
						<td>${modeloGenerico.getCampo2()}</td>
						<td><a
							href="/TiagoWebApp/novo/edit/${modeloGenerico.getId()}">editar</a></td>
						<td><a
							href="/TiagoWebApp/novo/delete/${modeloGenerico.getId()}">deletar</a></td>
					</tr>
				</c:forEach>
			</table>
		<footer>
		<p>&copy; Company 2016</p>
		</footer>
	</div>
	<!-- /container -->

</body>
</html>